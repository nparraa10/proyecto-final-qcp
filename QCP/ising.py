import numpy as np
from qutip import *
from typeguard import typechecked
from typing import List
from qiskit.chemistry import FermionicOperator
from qiskit.aqua.algorithms import VQE
from qiskit.aqua.components.optimizers import COBYLA, SLSQP
from qiskit.circuit.library import EfficientSU2
from qiskit.chemistry.components.initial_states import HartreeFock
from qiskit.aqua.operators import Z2Symmetries
from qiskit import BasicAer, IBMQ
from qiskit.aqua import QuantumInstance
from qiskit.chemistry.components.variational_forms import UCCSD

class Ising_Hamiltonian():
    '''
    Class to build a Hamiltonian 1D ising and solve this
    '''
    @typechecked
    def __init__(self,N:int):
        '''
        Parameters
        ----------
        N: int
            size of ising chain
        '''
        self._size = N
        self._hamiltonian = None
        self._eigenvalues = None
        self._eigenvectors = None
        self._minenergy_exact = None
        self._minenergy_vqe = None
        self._ev_energy = None
        self._counts = None

    @property
    def ev_energy(self):
        return self._ev_energy 
    
    @property
    def counts(self):
        return self._counts 

    @property
    def size(self):
        return self._size

    @property
    def hamiltonian(self):
        return self._hamiltonian

    @property
    def eigenvalues(self):
        return self._eigenvalues
    
    @property
    def eigenvectors(self):
        return self._eigenvectors

    @property
    def minenergy_exact(self):
        return self._minenergy_exact

    @property
    def minenergy_vqe(self):
        return self._minenergy_vqe
    
    @typechecked
    def hamiltonian_fit(self,J:float,h:float):
        '''
        Function that bulid the hamiltonian of ising with tranversal field 
        https://en.wikipedia.org/wiki/Transverse-field_Ising_model

        Parameters
        ----------
        J: float
            interacition between the spins
        h: float
            
        '''
        si = qeye(2)
        sz = sigmaz()
        sx = sigmax()

        sz_list = []
        sx_list = []

        for n in range(self._size):
            op_list = []
            for m in range(self._size):
                op_list.append(si)

            op_list[n] = sz
            sz_list.append(tensor(op_list))
            op_list[n] = sx
            sx_list.append(tensor(op_list))

        # construct the hamiltonian
        H = 0

        # energy splitting terms
        for n in range(self._size):
            H += - h * sx_list[n]

        # interaction terms
        for n in range(self._size-1):
            H += - J * sz_list[n] * sz_list[n+1]

        self._hamiltonian = H
        print(H)

    def solve_exact(self):
        '''
        Function to solve exactly the problem

        Note: the results are save in `self._eingenvalues` and `self._eigenvectors`
        '''
        print(self._hamiltonian)
        self._eigenvalues, self._eigenvectors = self.hamiltonian.eigenstates()
        self._minenergy_exact = min(self.eigenvalues)
    
    @typechecked
    def solve_vqe(self,maxiter : int = 400 ,qubit_mapping: str='jordan_wigner',type_run: str='simulator',save_convergence: bool = False,entanglement='linear'):
        '''
        Function to solve the system with a VQE 

        Parameters
        ----------
        maxiter: int
            Number of optimeze iterations 
        qubit_mapping: str #https://qiskit.org/documentation/apidoc/qiskit_chemistry.html read
            Type of mapping to quantum computer ("jordan_wigner", "parity", "bravyi_kitaev", "bksf")
        type_run: str
            Type of run the vqe, in a simulator `'simulator'` or a real device `'qc-real'`,
            default 'simulator'
        save_convergence: boolean
            If true, save the evolution of the energy in each iteration 
        entanglement:str
            Type of entanglement in variational form `'linear'` or `'full'` 
        '''
        #Also we can see this tutorial to other way to do this https://github.com/qiskit-community/qiskit-community-tutorials/blob/master/awards/teach_me_qiskit_2018/exact_ising_model_simulation/Ising_time_evolution.ipynb
        # We do this because take account it https://quantumcomputing.stackexchange.com/questions/13987/how-to-realize-su-schrieffer-heeger-model-in-qiskit
        fop = FermionicOperator(h1 = np.array(self.hamiltonian))

        optimizer = COBYLA(maxiter=maxiter, tol=0.0000001)

        qubitOp = fop.mapping(map_type=qubit_mapping, threshold=0.0001)
        qubitOp = Z2Symmetries.two_qubit_reduction(qubitOp, self.size)
        print('Number of qubits using',(qubitOp.num_qubits))

        var_form = EfficientSU2(qubitOp.num_qubits, entanglement=entanglement)
        #These ese the class of variational forms, we must build a wrapper of this  
        #https://qiskit.org/documentation/stubs/qiskit.aqua.components.variational_forms.VariationalForm.html#qiskit.aqua.components.variational_forms.VariationalForm

        if save_convergence:
            counts = []
            values = []
            def store_intermediate_result(eval_count, parameters, mean, std):
                counts.append(eval_count)
                values.append(mean)
            vqe = VQE(qubitOp, var_form, optimizer=optimizer, callback=store_intermediate_result,quantum_instance=QuantumInstance(backend=BasicAer.get_backend("qasm_simulator")))
            self._ev_energy = values
            self._counts = counts
        else:
            vqe = VQE(qubitOp,var_form, optimizer=optimizer)
        #how do we measurete the energy? https://qiskit.org/documentation/_modules/qiskit/aqua/algorithms/minimum_eigen_solvers/vqe.html
        #example of this in https://qiskit.org/textbook/ch-applications/vqe-molecules.html

        if type_run == 'simulator':
            if save_convergence:
                self._minenergy_vqe = np.real(vqe.compute_minimum_eigenvalue(operator=qubitOp)['eigenvalue'])
            else:
                backend = BasicAer.get_backend("qasm_simulator")
                self._minenergy_vqe = np.real(vqe.run(backend)['eigenvalue'])

        elif type_run == 'qc-real':
            IBMQ.load_account()
            provider = IBMQ.get_provider(hub='ibm-q')
            backend = provider.get_backend("ibmq_16_melbourne")
            device_backend = backend.configuration()
            coupling_map = device_backend.coupling_map
            quantum_instance = QuantumInstance(backend=backend, 
                                   shots=4192, 
                                   coupling_map=coupling_map)
            ret = vqe.run(quantum_instance)
            self._minenergy_vqe = np.real(ret['eigenvalue'])
        
        else:
            raise TypeError('Wrong type in type_run')