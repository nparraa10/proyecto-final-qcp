import sys
sys.path.append('../')
from QCP.ising import Ising_Hamiltonian

#Simple example of QCP ising 

ising = Ising_Hamiltonian(2) #Initiallize the ising

ising.hamiltonian_fit(J=1.0,h=1.0) #Give the parameters of hamiltonian

ising.solve_exact() #Solve through exact diagonalization 

print("These are the eingevalues",ising.eigenvalues)
print("These are the eigen vectors", ising.eigenvectors)