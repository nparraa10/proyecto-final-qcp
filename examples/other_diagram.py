import sys
from matplotlib import pyplot as plt
sys.path.append('../')
from QCP.ising import Ising_Hamiltonian
import time
import numpy as np

f = open("file_log_ener.txt", "w")
Js = np.linspace(0.05,1,20)
ising = Ising_Hamiltonian(3)
ex_energy = []
vqe_energy = []
for j in Js:
    start_time = time.time()
     #Initiallize the ising
    ising.hamiltonian_fit(J=j,h=0.06) #Give the parameters of hamiltonian
    ising.solve_exact() #Solve through exact diagonalization 
    ising.solve_vqe(maxiter = 350,qubit_mapping="jordan_wigner",entanglement='full',save_convergence = False) #Solve through variational quantum eigensolver

    exact = ising.minenergy_exact
    vqe_value = ising.minenergy_vqe
    ex_energy.append(exact)
    vqe_energy.append(vqe_value)

    f.write("This is the exact energy of the ground state {0} \n".format(exact))
    f.write("This is the vqe energy of the ground state {0} with linear entanglement \n".format(vqe_value))
    f.write("Porcentual difference {0} \n".format((vqe_value-exact)/exact*100))
    f.write("--- Seconds {0} \n".format((time.time() - start_time)))

f.close()

plt.plot(Js/0.06,ex_energy,'*',color='blue',label='Exact')
plt.plot(Js/0.06,vqe_energy,'+',color='red',label='VQE')
plt.title('Energy vs parameters')
plt.xlabel('J/h')
plt.ylabel('Energy')
plt.legend()
plt.savefig('energy_coupling.png')
plt.clf()