import sys
from matplotlib import pyplot as plt
sys.path.append('../')
from QCP.ising import Ising_Hamiltonian
import time

num_spins = [3,4]
f = open("file_log.txt", "w")
for spin in num_spins:
    start_time = time.time()

    ising = Ising_Hamiltonian(spin) #Initiallize the ising
    ising.hamiltonian_fit(J=0.5,h=0.06) #Give the parameters of hamiltonian

    ising.solve_exact() #Solve through exact diagonalization 
    ising.solve_vqe(maxiter = 350,qubit_mapping="jordan_wigner",entanglement='linear',save_convergence = True) #Solve through variational quantum eigensolver

    exact = ising.minenergy_exact
    vqe_value = ising.minenergy_vqe
    plt.plot(ising.counts,ising.ev_energy,label='linear',color='blue')
    
    f.write("---- To {0} spins ---- \n".format(spin))
    f.write("This is the exact energy of the ground state {0} \n".format(exact))
    f.write("This is the vqe energy of the ground state {0} with linear entanglement \n".format(vqe_value))
    f.write("Porcentual difference {0} \n".format((vqe_value-exact)/exact*100))

    ising.solve_vqe(maxiter = 350,qubit_mapping="jordan_wigner",entanglement='full',save_convergence = True) #Solve through variational quantum eigensolver
    vqe_value = ising.minenergy_vqe
    plt.plot(ising.counts,ising.ev_energy,label='full',color='red')

    f.write("This is the vqe energy of the ground state {0} with full entanglement \n".format(vqe_value))
    f.write("Porcentual difference {0} \n".format((vqe_value-exact)/exact*100))
    

    plt.title('Energy convergence')
    plt.xlabel('Iterations')
    plt.ylabel('Energy')
    plt.legend()
    plt.savefig('energy_iterations_{0}spins.png'.format(ising.size))
    plt.clf()
    f.write("--- Seconds {0} \n".format((time.time() - start_time)))
f.close()